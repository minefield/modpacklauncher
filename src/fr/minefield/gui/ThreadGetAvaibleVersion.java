package fr.minefield.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import fr.minefield.misc.Config;

public class ThreadGetAvaibleVersion implements Runnable {

    public ThreadGetAvaibleVersion(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void run() {
        try {
            URL urlAvaibleVersions = new URL(Config.getConf("URL_AVAIBLE_VERSIONS"));
            InputStreamReader is = new InputStreamReader(urlAvaibleVersions.openStream());
            BufferedReader in = new BufferedReader(is);

            String line = in.readLine();
            while (line != null) {
                mainWindow.addAvailableVersion(line);
                line = in.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MainWindow mainWindow;
}