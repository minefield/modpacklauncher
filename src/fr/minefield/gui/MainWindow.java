package fr.minefield.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import fr.minefield.misc.Config;
import fr.minefield.misc.MinecraftLauncherFinder;
import fr.minefield.misc.Util;

public class MainWindow extends JFrame {

    public MainWindow() {
        super(Config.getConf("CLIENT_NAME"));
        useSameMinecraftDirectory = true;
    }

    /**
     * Open window
     */
    public void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(new Dimension(854, 480)));
        setupLang();
        createWidgets();

        pack();
        // Center window on the screen
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Setup the buttons, and various widget. Design of application is here.
     */
    private void createWidgets() {
        JPanel mainWidgetsPanel = new JPanel();

        oneVersionAvailable = new JLabel();
        oneVersionAvailable.setText(Config.getConf("BASE_CLIENT_NAME"));
        oneVersionAvailable.setVisible(true);
        mainWidgetsPanel.add(oneVersionAvailable);

        chooseVersion = new JComboBox();
        chooseVersion.addItem(Config.getConf("BASE_CLIENT_NAME"));
        chooseVersion.setVisible(false);
        mainWidgetsPanel.add(chooseVersion);

        loadLauncherData();

        retriveAvaibleVersion();

        connectButton = new JButton(messages.getString("Connection"));
        connectButton.addActionListener(new ActionRunUpdate(this));
        mainWidgetsPanel.add(connectButton);

        downloadProgress = new JProgressBar(0, 100);
        mainWidgetsPanel.add(downloadProgress);

        forceUpdate = new JCheckBox();
        forceUpdate.setText(messages.getString("ForceUpdate"));
        mainWidgetsPanel.add(forceUpdate);

        labelMcLauncher = new JLabel();
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            MinecraftLauncherFinder.StartSearch(this);
            if (MinecraftLauncherFinder.searching) {
                labelMcLauncher.setText(messages.getString("McLauncherLabel") + messages.getString("Searching"));
            } else {
                labelMcLauncher.setText(messages.getString("McLauncherLabel") + messages.getString("NotFound"));
            }
        } else {
            labelMcLauncher.setText(messages.getString("McLauncherLabel") + minecraftLauncherPath);
        }
        mainWidgetsPanel.add(labelMcLauncher);

        buttonMcLauncher = new JButton();
        buttonMcLauncher.setText(messages.getString("SelectLauncher"));
        buttonMcLauncher.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                askMinecraftLauncherPath();
            }
        });
        mainWidgetsPanel.add(buttonMcLauncher);

        checkUseSameMinecraftDirectory = new JCheckBox();
        checkUseSameMinecraftDirectory.setText(messages.getString("useSameMinecraftDirectory"));
        checkUseSameMinecraftDirectory.setSelected(useSameMinecraftDirectory);
        mainWidgetsPanel.add(checkUseSameMinecraftDirectory);

        checkResetLauncherProfile = new JCheckBox();
        checkResetLauncherProfile.setText(messages.getString("resetLauncherProfile"));
        mainWidgetsPanel.add(checkResetLauncherProfile);

        checkLauncherVersion();

        this.add(mainWidgetsPanel);
    }

    public void notifyOutdated() {
        // FIXME
        System.out.println("Nouvelle version disponible a l'address: " + Config.getConf("LAUNCHER_GET_URL"));
    }

    /**
     * Called when the update fail, then it unlock interface and should display
     * message
     * 
     * @param message
     *            errorMessageToken (for localised string)
     */
    public void onFailUpdate(String message) {
        connectButton.setEnabled(true);
        chooseVersion.setEnabled(true);
        downloadProgress.setValue(0);
        forceUpdate.setEnabled(true);
        checkResetLauncherProfile.setEnabled(true);
        checkUseSameMinecraftDirectory.setEnabled(true);
        // FIXME display message
        messages.getString(message);
    }

    /**
     * Called when the update start, it lock all interface
     */
    public void onStartUpdate() {
        connectButton.setEnabled(false);
        chooseVersion.setEnabled(false);
        checkResetLauncherProfile.setEnabled(false);
        forceUpdate.setEnabled(false);
        checkUseSameMinecraftDirectory.setEnabled(false);
        saveLauncherData();
    }

    private void setupLang() {
        currentLocale = new Locale("fr", "FR");
        messages = ResourceBundle.getBundle("StringLangs", currentLocale);
    }
    
    public void addAvailableVersion(String line) {
        oneVersionAvailable.setVisible(false);
        chooseVersion.setVisible(true);
        chooseVersion.addItem(line);
        if (lastSelectedVersion != null && line.equals(lastSelectedVersion)) {
            chooseVersion.setSelectedIndex(chooseVersion.getItemCount() - 1);
        }
    }

    public String getSelectedVersion() {
        return (String) chooseVersion.getSelectedItem();
    }

    /**
     * Save preference of this launcher into a file stored in client directory.
     * 
     * ${HOME}/.XXXXX/prelauncher.properties
     * %APPDATA%/.XXXXX/prelauncher.properties
     */
    private void saveLauncherData() {
        File clientDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        if (!clientDirectory.exists()) {
            clientDirectory.mkdir();
        }

        try {
            File prelauncherProperties = new File(clientDirectory, "prelauncher.properties");
            Properties properties = new Properties();

            properties.setProperty("lastVersion", (String) chooseVersion.getSelectedItem());

            if (minecraftLauncherPath != null && minecraftLauncherPath.exists()) {
                properties.setProperty("mcLauncher", minecraftLauncherPath.toString());
            }

            useSameMinecraftDirectory = checkUseSameMinecraftDirectory.isSelected();
            properties.setProperty("useSameMinecraftDirectory", useSameMinecraftDirectory ? "true" : "false");

            FileOutputStream fos = new FileOutputStream(prelauncherProperties);
            properties.store(fos, null);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load saved preference of this launcher from file stored in client directory.
     * 
     * ${HOME}/.XXXXX/prelauncher.properties
     * %APPDATA%/.XXXXX/prelauncher.properties
     */
    private void loadLauncherData() {
        File clientDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        if (clientDirectory.exists()) {
            try {
                File prelauncherProperties = new File(clientDirectory, "prelauncher.properties");
                if (prelauncherProperties.exists()) {
                    FileInputStream fis = new FileInputStream(prelauncherProperties);
                    Properties properties = new Properties();
                    properties.load(fis);

                    lastSelectedVersion = properties.getProperty("lastVersion");

                    minecraftLauncherPath = null;
                    String mcLauncher = properties.getProperty("mcLauncher");
                    if (mcLauncher != null) {
                        minecraftLauncherPath = new File(mcLauncher);
                    }

                    String useMcDir =  properties.getProperty("useSameMinecraftDirectory");
                    useSameMinecraftDirectory = false;
                    if (useMcDir != null) {
                        useSameMinecraftDirectory = useMcDir.equals("true");
                    }

                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Start a thread that will download list of avaible version, and fill the
     * comboBox with each.
     */
    private void retriveAvaibleVersion() {
        Thread thread = new Thread(new ThreadGetAvaibleVersion(this));
        thread.start();
    }

    private void checkLauncherVersion() {
        Thread thread = new Thread(new ThreadCheckLaucherVersion(this));
        thread.start();
    }

    public void updateProgress(int i) {
        downloadProgress.setValue(i);
    }

    public void UpdateMinecraftLauncherPath() {
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            if (!MinecraftLauncherFinder.searching) {
                minecraftLauncherPath = MinecraftLauncherFinder.GetMinecraftLauncherPath();
                updateDisplayMcLauncherPath();
            }
        }
    }

    private void askMinecraftLauncherPath() {
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            if (!MinecraftLauncherFinder.searching) {
                minecraftLauncherPath = MinecraftLauncherFinder.GetMinecraftLauncherPath();
                if (minecraftLauncherPath != null && minecraftLauncherPath.exists()) {
                    updateDisplayMcLauncherPath();
                    return;
                }
            }
        }

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new MinecraftLauncherFilter());
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            minecraftLauncherPath = fc.getSelectedFile();
        }
        updateDisplayMcLauncherPath();
    }

    private void updateDisplayMcLauncherPath() {
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            labelMcLauncher.setText(messages.getString("McLauncherLabel") + messages.getString("NotFound"));
        } else {
            labelMcLauncher.setText(messages.getString("McLauncherLabel") + minecraftLauncherPath);
            saveLauncherData();
        }
    }

    public File getLauncherPath() {
        if (minecraftLauncherPath == null) {
            askMinecraftLauncherPath();
            if (minecraftLauncherPath == null) {
                return null;
            }
        }
        if (!minecraftLauncherPath.exists()) {
            askMinecraftLauncherPath();
        } else {
            return minecraftLauncherPath;
        }
        return null;
    }

    public boolean forceUpdate() {
        return forceUpdate.isSelected();
    }

    public boolean isUseSameMinecraftDirectory() {
        return useSameMinecraftDirectory;
    }

    public boolean isResetLauncherProfile() {
        return checkResetLauncherProfile.isSelected();
    }

    public String getLastSelectedVersion() {
        return lastSelectedVersion;
    }

    private static final long serialVersionUID = -3964501094848191700L;
    private File minecraftLauncherPath;
    private JComboBox chooseVersion;
    private JLabel oneVersionAvailable;
    private JButton connectButton;
    private JProgressBar downloadProgress;
    private String lastSelectedVersion;
    private Locale currentLocale;
    private ResourceBundle messages;
    private JCheckBox forceUpdate;
    private JButton buttonMcLauncher;
    private JLabel labelMcLauncher;
    private boolean useSameMinecraftDirectory;
    private JCheckBox checkUseSameMinecraftDirectory;
    private JCheckBox checkResetLauncherProfile;
}
