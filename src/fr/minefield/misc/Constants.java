package fr.minefield.misc;

public class Constants {
    public final static int BUFFER_SIZE = 65536;
    public final static int LAUNCHER_VERSION = 9;
}
